create table tbl_categories
(
    id int auto_increment
        primary key,
    user_id varchar(15) null,
    category_name varchar(400) null,
    created_at timestamp default CURRENT_TIMESTAMP null
);

create table tbl_data
(
    id int auto_increment
        primary key,
    content_data json null,
    category_id int null,
    user_id varchar(15) null,
    created_at timestamp default CURRENT_TIMESTAMP null
);

create table tbl_users
(
    id int auto_increment
        primary key,
    user_id varchar(15) null,
    other_data json null,
    join_date timestamp default CURRENT_TIMESTAMP null,
    update_time timestamp default CURRENT_TIMESTAMP null,
    type int default 0 null comment '0:user 1:admin',
    constraint tbl_users_user_id_uindex
        unique (user_id)
);

