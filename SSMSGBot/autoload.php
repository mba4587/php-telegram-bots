<?php
global $config;
/// Configuration of the code
header("Content-type: text/html; charset=utf-8");
global $mysqli;
$mysqli = new mysqli($config['database']['host'], $config['database']['username'], $config['database']['password'], $config['database']['name']);
$mysqli->set_charset("utf8");

define("BOT_TOKEN", $config['bot']['token']);
define("BOT_USERNAME", $config['bot']['username']);
define("PROXY", $config['proxy']['status'] ? ['type' => CURLPROXY_SOCKS5_HOSTNAME, 'port' => $config['proxy']['port'], 'url' => $config['proxy']['url']] : []);

require_once "Telegram.php";
$proxy = PROXY;
$tg = new Telegram(BOT_TOKEN, false, $proxy);

if ($config['bot']['encoded_updates'] == true) {
    $update = json_decode(base64_decode(file_get_contents('php://input')));
} else {
    $update = json_decode(file_get_contents('php://input'));
}


$strings = json_decode(file_get_contents($config['language']), true);
/// Configuration of the code


require_once "src/functions.php";