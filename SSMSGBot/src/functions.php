<?php

function answerEmptyInlineQuery()
{
    global $mysqli, $id, $tg, $stringsMessages, $stringsButtons, $userId;
    $res = $mysqli->query("SELECT id, FROM_BASE64(category_name) as name FROM tbl_categories WHERE user_id = '$userId'");
    if ($res->num_rows == 0) {
        $tg->answerInlineQuery([
            "inline_query_id" => $id,
            "switch_pm_text" => $stringsButtons['create_category'],
            "switch_pm_parameter" => "start",
            "cache_time" => 10
        ]);
    } else {
        $results = [];
        while ($row = $res->fetch_assoc()) {
            $results[] = [
                "type" => "article",
                "id" => md5(microtime()),
                "title" => $row['name'] . " - " . $row['id'] ,
                "input_message_content" => [
                    "message_text" => sprintf($stringsMessages['category_name'], $row['name'] . " - [ID: {$row['id']}]"),
                    "parse_mode"=>"HTML"
                ]
            ];
        }
        $tg->answerInlineQuery([
            "inline_query_id" => $id,
            "results" => json_encode($results),
            "switch_pm_text" => $stringsButtons['create_category'],
            "switch_pm_parameter" => "start",
            "cache_time" => 10
        ]);
    }
}

function makeItTheSame($string)
{
    return strtolower(trim($string));
}

function markupCreator($keyboard, $type = "keyboard")
{
    if ($type == "keyboard") {
        return json_encode([
            "keyboard" => $keyboard,
            "resize_keyboard" => true
        ]);
    }
    return false;
}

function getUserData($userId)
{
    global $mysqli;
    $userId = $mysqli->escape_string($userId);
    $res = $mysqli->query("SELECT id FROM tbl_users");
    $admin = 0;
    if($res->num_rows == 0) {
        $admin = 1;
    }
    $res = $mysqli->query("SELECT other_data FROM tbl_users WHERE user_id = '$userId'");
    if ($res->num_rows == 0) {
        $other_data = json_encode([
            "step" => "none"
        ]);
        $mysqli->query("INSERT INTO tbl_users (user_id, other_data, type) VALUE ('$userId', '$other_data', '$admin')");
    } else {
        $row = $res->fetch_assoc();
        $other_data = $row['other_data'];
    }
    return json_decode($other_data, true);
}

function changeUserData($userId, $data = [])
{
    global $mysqli;
    $userId = $mysqli->escape_string($userId);

    $res = $mysqli->query("SELECT other_data FROM tbl_users WHERE user_id = '$userId'");
    if ($res->num_rows == 1) {
        $row = $res->fetch_assoc();
        $other_data = json_decode($row['other_data'], true);
        $other_data = array_merge($other_data, $data);
        $other_data_json = json_encode($other_data);
        $mysqli->query("UPDATE tbl_users SET update_time = current_timestamp, other_data = '$other_data_json' WHERE user_id = '$userId'");
    } else {
        return false;
    }
    return $other_data;
}

function removeKeyboard()
{
    return json_encode([
        'remove_keyboard' => true
    ]);
}