<?php
global $update, $mysqli, $stringsButtons, $stringsMessages, $tg;
$message = ($update->message);
$userId = $mysqli->real_escape_string($message->from->id);
$firstName = $mysqli->real_escape_string($message->from->first_name);
$chatId = $mysqli->real_escape_string($message->chat->id);
$text = $mysqli->real_escape_string($message->text);

$userData = getUserData($userId);
$mainButton = markupCreator([
    [
        ["text" => $stringsButtons['create_category']],
        ["text" => $stringsButtons['delete_category']]
    ]
]);

switch (makeItTheSame($text)) {
    case makeItTheSame($stringsButtons['cancel']):
    case "/start":
        start:
        if (makeItTheSame($stringsButtons['cancel']) == makeItTheSame($text) || makeItTheSame($text) == "/start" || makeItTheSame($text) == "/reset") {
            changeUserData($userId, [
                "step" => "none",
                "data" => [],
                "text" => ""
            ]);
        }
        $res = $tg->sendMessage([
            "chat_id" => $userId,
            "text" => sprintf($stringsMessages['start'], $firstName, BOT_USERNAME),
            "reply_markup" => $mainButton
        ]);
        break;
    case makeItTheSame('/start start'):
    case makeItTheSame($stringsButtons['create_category']):
        $tg->sendMessage([
            "chat_id" => $userId,
            "text" => $stringsMessages['create_category']['step_one'],
            "reply_markup" => markupCreator([[["text" => $stringsButtons['cancel']]]])
        ]);
        changeUserData($userId, [
            "step" => "create_category",
        ]);
        break;
    case makeItTheSame('/help'):
        $tg->sendMessage([
            "chat_id" => $userId,
            "text" => $stringsMessages['help']
        ]);
        break;
    case "/owner":
        $tg->sendMessage([
            "chat_id" => $userId,
            "text" => "Created by <a href='https://t.me/mba4587'>MBA</a>",
            "parse_mode" => "HTML",
        ]);
        break;
    case makeItTheSame($stringsButtons['delete_category']):
        $res = $mysqli->query("SELECT id, FROM_BASE64(category_name) as name FROM tbl_categories WHERE user_id = '$userId'");
        if ($res->num_rows == 0) {
            $tg->sendMessage([
                "chat_id" => $userId,
                "text" => $stringsMessages['delete_category']['no_category_registered'],
                "reply_markup" => markupCreator([
                    [["text" => $stringsButtons['create_category']]]
                ])
            ]);
        } else {
            changeUserData($userId, [
                "step" => "delete_category"
            ]);
            $buttons = [];
            while ($row = $res->fetch_assoc()) {
                $buttons[] = ["text" => $row['name']];
            }
            $buttons[] = ["text" => $stringsButtons['cancel']];
            $buttons = array_chunk($buttons, 3);
            $tg->sendMessage([
                "chat_id" => $userId,
                "text" => $stringsMessages['delete_category']['select'],
                "reply_markup" => markupCreator($buttons)
            ]);
        }
        break;
    default:
        switch ($userData['step']) {
            case "create_category":
                if (mb_strlen($text) <= 100) {
                    $res = $mysqli->query("SELECT id FROM tbl_categories WHERE user_id = '$userId' AND FROM_BASE64(category_name) = '$text'");
                    if ($res->num_rows == 0) {
                        /// Insert and get id
                        $mysqli->query("INSERT INTO tbl_categories (user_id, category_name) VALUE ('$userId', TO_BASE64('$text'))");
                        $res = $mysqli->query("SELECT id FROM tbl_categories WHERE user_id = '$userId' AND category_name = TO_BASE64('$text')");
                        $row = $res->fetch_assoc();
                        $id = $row['id'];
                        /// Change step and notify user
                        changeUserData($userId, ['step' => 'none']);
                        $tg->sendMessage([
                            "chat_id" => $userId,
                            "text" => sprintf($stringsMessages['create_category']['success'], BOT_USERNAME, $text . " - $id"),
                            "reply_markup" => json_encode([
                                'inline_keyboard' => [
                                    [['text' => $stringsButtons['switch_inline'], "switch_inline_query" => $id]]
                                ]
                            ])
                        ]);
                        goto start;
                    } else {
                        $tg->sendMessage([
                            "chat_id" => $userId,
                            "text" => ($stringsMessages['create_category']['this_name_exist']),
                        ]);
                    }
                } else {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => $stringsMessages['create_category']['length_error']
                    ]);
                }
                break;
            case "delete_category":
                $res = $mysqli->query("SELECT id FROM tbl_categories WHERE category_name = TO_BASE64('$text') AND user_id = '$userId'");
                if ($res->num_rows == 1) {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => sprintf($stringsMessages['delete_category']['are_u_sure'], $text),
                        "reply_markup" => markupCreator([
                            [['text' => $stringsButtons['delete_confirm']]],
                            [['text' => $stringsButtons['cancel']]]
                        ])
                    ]);
                    changeUserData($userId, [
                        "step" => "delete_category_step_two",
                        "text" => base64_encode($text)
                    ]);
                } else {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => $stringsMessages['delete_category']['category_not_founded'],
                        "reply_markup" => $mainButton
                    ]);
                }
                break;
            case "delete_category_step_two":
                if (makeItTheSame($stringsButtons['delete_confirm']) == makeItTheSame($text)) {
                    $categoryName = base64_decode($userData['text']);
                    $mysqli->query("DELETE FROM tbl_categories WHERE category_name = TO_BASE64('$categoryName') AND user_id = '$userId'");
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => sprintf($stringsMessages['delete_category']['category_delete_success'], $categoryName),
                        "reply_markup" => removeKeyboard()
                    ]);
                } else {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => $stringsMessages['delete_category']['delete_process_canceled'],
                        "reply_markup" => removeKeyboard()
                    ]);
                    changeUserData($userId, [
                        "step" => "none",
                        "text" => ''
                    ]);
                }
                goto start;
                break;
            case "select_category":
                $res = $mysqli->query("SELECT id FROM tbl_categories WHERE category_name = TO_BASE64('$text') AND user_id = '$userId'");
                if ($res->num_rows == 1) {
                    $row = $res->fetch_assoc();
                    $categoryId = $row['id'];
                    $data = json_encode($userData['data']);
                    $mysqli->query("INSERT INTO tbl_data (content_data, category_id, user_id) VALUE ('$data', '$categoryId', '$userId')");

                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => sprintf($stringsMessages['insert_data_to_category']['data_added_successful'], $text),
                        "reply_markup" => json_encode([
                            'inline_keyboard' => [
                                [['text' => "Switch Inline", "switch_inline_query" => $text]]
                            ]
                        ])
                    ]);
                    changeUserData($userId, [
                        "step" => "none",
                        "data" => []
                    ]);
                    goto start;
                } else {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => $stringsMessages['insert_data_to_category']['category_not_founded'],
                    ]);
                }
                break;
            case "none":
                $ok = true;
                if (isset($message->text)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "text",
                            "title" => base64_encode(substr("$text", 0, 100) . "..."),
                            "content" => base64_encode($text)
                        ])
                    ]);
                } else if (isset($message->photo)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "photo",
                            "caption" => base64_encode($message->caption),
                            "title" => base64_encode(rand(100000, 99999999)),
                            "width" => $message->photo[count($message->photo) - 1]->width,
                            "height" => $message->photo[count($message->photo) - 1]->height,
                            "file_unique_id" => $message->photo[count($message->photo) - 1]->file_unique_id,
                            "file_id" => $message->photo[count($message->photo) - 1]->file_id
                        ]),
                    ]);
                } else if (isset($message->video)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "video",
                            "caption" => base64_encode($message->caption),
                            "title" => base64_encode($message->video->file_name),
                            "width" => $message->video->width,
                            "height" => $message->video->height,
                            "duration" => $message->video->duration,
                            "file_unique_id" => $message->video->file_unique_id,
                            "file_id" => $message->video->file_id
                        ]),
                    ]);
                } else if (isset($message->document)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "document",
                            "caption" => base64_encode($message->caption),
                            "title" => base64_encode($message->document->file_name),
                            "file_unique_id" => $message->document->file_unique_id,
                            "file_id" => $message->document->file_id
                        ]),
                    ]);
                } else if (isset($message->audio)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "audio",
                            "caption" => base64_encode($message->caption),
                            "file_name" => $message->audio->file_name,
                            "title" => base64_encode($message->audio->title),
                            "file_unique_id" => $message->audio->file_unique_id,
                            "file_id" => $message->audio->file_id
                        ]),
                    ]);
                } else if (isset($message->sticker)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "sticker",
                            "caption" => base64_encode(""),
                            "set_name" => $message->sticker->set_name,
                            "title" => base64_encode($message->sticker->set_name),
                            "file_unique_id" => $message->sticker->file_unique_id,
                            "emoji" => $message->sticker->emoji,
                            "width" => $message->sticker->width,
                            "height" => $message->sticker->height,
                            "file_id" => $message->sticker->file_id
                        ]),
                    ]);
                } else if (isset($message->voice)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "sticker",
                            "caption" => base64_encode(date("now")),
                            "title" => base64_encode("Voice - " . $message->voice->duration),
                            "duration" => $message->voice->duration,
                            "file_unique_id" => $message->voice->file_unique_id,
                            "file_id" => $message->audio->file_id
                        ]),
                    ]);
                } else if (isset($message->location)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "location",
                            "description" => base64_encode("(" . $message->location->latitude . "," . $message->location->longitude . ")"),
                            "title" => base64_encode("Location"),
                            "lat" => $message->location->latitude,
                            "long" => $message->location->longitude,
                        ]),
                    ]);
                } else if (isset($message->contact)) {
                    changeUserData($userId, [
                        "step" => "select_category",
                        "data" => ([
                            "type" => "contact",
                            "title" => base64_encode($message->contact->first_name . " - " . $message->contact->phone_number),
                            "first_name" => $message->contact->first_name,
                            "last_name" => $message->contact->last_name,
                            "phone_number" => $message->contact->phone_number,
                        ]),
                    ]);
                } else {
                    $ok = false;
                }
                if ($ok == true) {
                    $res = $mysqli->query("SELECT id, FROM_BASE64(category_name) as name FROM tbl_categories WHERE user_id = '$userId'");
                    if ($res->num_rows == 0) {
                        $tg->sendMessage([
                            "chat_id" => $userId,
                            "text" => $stringsMessages['insert_data_to_category']['no_category_registered'],
                            "reply_markup" => markupCreator([
                                [["text" => $stringsButtons['create_category']]]
                            ])
                        ]);
                    } else {
                        $buttons = [];
                        while ($row = $res->fetch_assoc()) {
                            $buttons[] = ["text" => $row['name']];
                        }
                        $buttons[] = ["text" => $stringsButtons['cancel']];
                        $buttons = array_chunk($buttons, 2);
                        $tg->sendMessage([
                            "chat_id" => $userId,
                            "text" => $stringsMessages['insert_data_to_category']['select'],
                            "reply_markup" => markupCreator($buttons)
                        ]);
                    }
                } else {
                    $tg->sendMessage([
                        "chat_id" => $userId,
                        "text" => $stringsMessages['this_type_not_support']
                    ]);
                }
                break;
        }
        break;
}