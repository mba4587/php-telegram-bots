<?php

global $update, $mysqli, $tg;

$inline_query = $update->inline_query;
$userId = $inline_query->from->id;
$query = trim($inline_query->query);
$id = $inline_query->id;
$offset = $inline_query->offset == null ? 0 : $inline_query->offset;

if ($query == "") {
    answerEmptyInlineQuery();
} else {
    $res = $mysqli->query("SELECT id, FROM_BASE64(category_name) as name FROM tbl_categories WHERE FROM_BASE64(category_name) = '$query' OR id = '$query' AND user_id = '$userId' LIMIT 1");
    if ($res->num_rows == 0) {
        answerEmptyInlineQuery();
    } else {
        $row = $res->fetch_assoc();
        $res = $mysqli->query("SELECT id, content_data FROM tbl_data WHERE category_id = '{$row['id']}' LIMIT $offset,10");
        if ($res->num_rows == 0) {
            $offset = '';
        } else {
            $offset += 10;
        }

        $resultsInline = [];
        while ($row = $res->fetch_assoc()) {
            $data = json_decode($row['content_data'], true);
            if ($data['type'] == "text") {
                $resultsInline[] = [
                    "type" => "article",
                    "id" => base64_encode(rand(100, 99999)),
                    "title" => str_replace("\n", "",base64_decode($data['title'])),
                    "input_message_content" => [
                        "message_text" =>base64_decode( $data['content']),
                        "parse_mode" => "HTML"
                    ]
                ];
            } if ($data['type'] == "location") {
                $resultsInline[] = [
                    "type" => "location",
                    "id" => base64_encode(rand(100, 99999)),
                    "title" => str_replace("\n", "",base64_decode($data['title'])),
                    "description" => str_replace("\n", "",base64_decode($data['description'])),
                    "latitude"=>$data['lat'],
                    "longitude"=>$data['long']
                ];
            } else if ($data['type'] == "contact") {
                $resultsInline[] = [
                    "type" => "contact",
                    "id" => base64_encode(rand(100, 99999)),
                    "title" => str_replace("\n", "",base64_decode($data['title'])),
                    "phone_number"=>$data['phone_number'],
                    "first_name"=>$data['first_name'],
                    "last_name"=>$data['last_name']
                ];
            } else {
                $resultsInline[] = [
                    "type" => $data['type'],
                    "id" => base64_encode(rand(100, 99999)),
                    "{$data['type']}_file_id" => $data['file_id'],
                    "caption" => ($data['caption']) == null ? "" : base64_decode($data['caption']),
                    "title" => ($data['title']) == null ? "" : str_replace("\n", "",base64_decode($data['title']))
                ];
            }
        }

        $tg->answerInlineQuery([
            "inline_query_id" => $id,
            "results" => json_encode($resultsInline),
            "next_offset" => $offset,
            "cache_time" => 10,
        ]);
    }
}
function get64BitHash($str)
{
    return gmp_strval(gmp_init(substr(md5($str), 0, 16), 16), 10);
}