<?php
$config = json_decode(file_get_contents("config.json"), true);
if (isset($config['bot']['token'])) {
    require_once "autoload.php";
    global $tg, $update, $strings, $mysqli;

    $stringsMessages = $strings['messages'];
    $stringsButtons = $strings['buttons'];

    if (isset($update->message)) {
        require_once "src/message.php";
    } else if (isset($update->inline_query)) {
        require_once "src/inline_query.php";
    }
} else {
    die("What do you want from me :( set your token first.");
}
